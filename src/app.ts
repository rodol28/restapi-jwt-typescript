import express, { Application } from 'express';
import routesAuth from './routes/auth';
import morgan from 'morgan';

const app: Application = express();
import './database';

// setting
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// routes
app.use('/api/auth',routesAuth);

export default app;