import { Router } from 'express';
import { authController } from '../controllers/Auth.ctrl';
import { TokenValidation  } from '../libs/verifyToken';
const router: Router = Router();

router.post('/signin', authController.signin);
router.post('/signup', authController.signup);
router.get('/profile', TokenValidation, authController.profile);

export default router;