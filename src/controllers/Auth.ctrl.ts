import { Request, Response } from 'express';
import User, { IUser } from '../models/User';

import jwt from 'jsonwebtoken';

class AuthController {

    public async signup(req: Request, res: Response) {
        // saving new user
        const user: IUser = new User({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        });
        // encrypt password
        user.password = await user.encryptPassword(user.password);
        
        // save user
        const saveUser = await user.save();

        // generate token
        const token: string = jwt.sign({_id: saveUser._id}, process.env.TOKEN_SECRET || 'tokentest');
        
        // send token
        res.header('auth-token', token).json(saveUser);
    }

    public async signin(req: Request, res: Response) {
        
        const user = await User.findOne({email: req.body.email});
        if(!user) return res.status(400).json('Email or password is wrong'); 
        const correctPassword: boolean = await user.validatePassword(req.body.password);
        if(!correctPassword) return res.status(400).json('Invalid password');

        const token: string = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET || 'tokentest', {
            expiresIn: Math.floor(Date.now() / 1000) + (60 * 60)
        });

        res.header('auth-token', token).json(user);
    }

    public profile(req: Request, res: Response) {

        res.send('profile');
    }
}


export const  authController = new AuthController();

